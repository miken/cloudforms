# Sets a new expanded disk size for a VMware disk
#
# PARAMETERS
#   $evm.root['vm'] - VM to resize the disk of.
#   $evm.root['dialog_disk_name'] - Name of the disk to resize (e.g. "Hard disk 2")
#   $evm.root['dialog_delta_size_in_gb'] - The increase in size (in GiB) to make the disk
#
@DEBUG = false
require 'rbvmomi'

def dump_root()
  $evm.log(:info, "Root:<$evm.root> Begin $evm.root.attributes")
  $evm.root.attributes.sort.each { |k, v| $evm.log(:info, "Root:<$evm.root> Attribute - #{k}: #{v}")}
  $evm.log(:info, "Root:<$evm.root> End $evm.root.attributes")
  $evm.log(:info, "")
end

def recursive_find_vm(folder, name, exact = false)
  found = []
  folder.children.each do |child|
    if matches(child, name, exact)
      found << child
    elsif child.class == RbVmomi::VIM::Folder
      found << recursive_find_vm(child, name, exact)
    end
  end
  found.flatten
end

def matches(child, name, exact = false)
  is_vm = child.class == RbVmomi::VIM::VirtualMachine
  name_matches = (name == "*") || (exact ? (child.name == name) : (child.name.include? name))
  return is_vm && name_matches
end

def list_disks(vim_vm)
  $evm.log(:info, "VM has disks: #{vim_vm.disks.inspect}")
end

def resize_disk(vim_vm, disk_name, new_capacity_in_kb)
  disk = vim_vm.config.hardware.device.grep(RbVmomi::VIM::VirtualDisk).find { |x| x.deviceInfo.label == disk_name }
  $evm.log(:info, "Disk: #{disk.deviceInfo.label}, new size (KB): #{new_capacity_in_kb}")
  disk.capacityInKB = new_capacity_in_kb
  vm_cfg = {
    :deviceChange => [{
      :device    => disk,
      :operation => :edit
    }]
  }
  vim_vm.ReconfigVM_Task(:spec => vm_cfg).wait_for_completion
end

def find_vm(vm)
  ems = vm.ext_management_system
  vim = RbVmomi::VIM.connect(:host     => ems.ipaddress || ems.hostname, 
                             :user     => ems.authentication_userid, 
                             :password => ems.authentication_password, 
                             :insecure => true)
  dc = vim.rootFolder.childEntity.first
  recursive_find_vm(dc.vmFolder, vm.name).first
end

dump_root() if @DEBUG
vm               = $evm.root['vm']
disk_name        = $evm.root['dialog_disk_name'] 
delta_size_in_gb = $evm.root['dialog_delta_size_in_gb'].to_i

vim_vm = find_vm(vm)
unless vim_vm.nil?
  disk = $evm.vmdb(:disk).where(["hardware_id = ? AND device_name = ?", vm.hardware.id, disk_name]).first or 
         raise "Can't find disk #{$evm.root['dialog_disk_name']} for VM #{vm.name}"
  existing_capacity_in_kb = disk.size / 1024
  new_capacity_in_kb = existing_capacity_in_kb + (delta_size_in_gb * (1024**2))
  resize_disk(vim_vm, disk_name, new_capacity_in_kb)
end
