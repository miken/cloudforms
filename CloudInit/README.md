# Cloud-Init

The [cloud-init](https://cloudinit.readthedocs.io/en/latest/) scripts in this directory are used by several Service Catalog Items.

* RHEL7CentrallyManaged: this cloud-init script will create a user specified account and will allow the user with the specified SSH key to login. This demonstrates the scenario where the end user has limited access and the VM is centrally managed by the Service Provider or IT Department.

* RHEL7SelfManaged: this cloud-init script will add the provided SSH key to the root and cloud-user account. This demonstrates the scenario where the end user will have full control over the VM.