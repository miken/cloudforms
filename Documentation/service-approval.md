# Service Approval

This Automate Domain contains code imported from [Kevin Morey's Github Repository](https://github.com/ramrexx). The Code will require approval for the flavors "m1.medium" and "m1.large", but it will automatically approve all other flavors.

## Purpose

This use case will demonstrate automatic approval processes in CloudForms. If the user orders a small VM, the request will be automatically approved, medium and large will require manual approval by an Administrator.

## Execution

1. log in as user "user"

1. Order from the Service Catalog the item "RHEL7 Self Managed" on OpenStack

1. choose flavor "small" to show auto approval

1. do the same again and choose flavor "medium" or "large"

1. show the request will stop at "pending request"

1. approve the request as an Administrator

1. demonstrate that the request will continue (or fail if the request was denied)
